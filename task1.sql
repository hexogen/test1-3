SELECT 
    u.name AS name, GROUP_CONCAT(t.tag) AS tag
FROM
    `USERS` AS u
        INNER JOIN
    USER_TAGS AS t ON u.userID = t.userId
GROUP BY u.userID;
