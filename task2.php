<?php

/*
 * 3 types of str_reverse that came to my mind
 *   1: Crete a new buffer and push all letters to it in reverse order
 *     Time complexity O(N)
 *     Wasted space O(N) But its ok if string is immutable so we just cant avoid it
 *   2: Create 2 iterators (forward and backward) and swap all letters with theirs counterparts
 *     Tome complexity O(N)
 *     Wasted space O(1)
 *   3: use build in function :) To be honest, I just dont know any other good enough method
 *     Tome complexity O(N)
 */

/**
 * !!! WARNING: Current implementation does not work correctly with unicode
 * @param string $str
 * @return string
 */
function str_reverse(string $str): string {
    $length = strlen($str);

    if ($length == 0) {
        return '';
    }

    for ($i  = 0, $j = $length - 1; $i < $j ; $i++, $j--) {
        $swap = $str[$i];
        $str[$i] = $str[$j];
        $str[$j] = $swap;
    }

    return $str;
}

//tests
$str = '';
$res = str_reverse($str);
assert($res === '', 'empty string test');

$str = '1';
$res = str_reverse($str);
assert($res === '1', '1 letter test');

$str = '23';
$res = str_reverse($str);
assert($res === '32', '2 letters test');

$str = '1234567890';
$res = str_reverse($str);
assert($res === '0987654321', '10 letters test');

$str = '100000';
$res = str_reverse($str);
assert($res === '000001', 'test only first is different');

$str = '000001';
$res = str_reverse($str);
assert($res === '100000', 'test only last is different');

$str = 'abcdefghijklmnopqrsturvwxyz';
$res = str_reverse($str);
assert($res === 'zyxwvrutsrqponmlkjihgfedcba', 'alphabet reverse letters test');

$str = '123';
$res = str_reverse($str);
assert($res === '321', 'prime number length 1');

$str = '1234567';
$res = str_reverse($str);
assert($res === '7654321', 'prime number length 2');

$str = '12345678901';
$res = str_reverse($str);
assert($res === '10987654321', 'prime number length 3');

$str = '1234567890123';
$res = str_reverse($str);
assert($res === '3210987654321', 'prime number length 4');