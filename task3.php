<?php

function hailstone(int $a): int {
    $steps = 0;

    while ($a !== 1) {
        if ($a % 2 === 0) {
            $a /= 2;
        } else {
            $a = 3 * $a + 1;
        }

        $steps++;
    }

    return $steps;
}

//tests
$r = hailstone(10);
assert($r === 6);

// 9 28 14 7 22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1
$r = hailstone(9);
assert($r === 19);

// 1
$r = hailstone(1);
assert($r === 0);

// 2 1
$r = hailstone(2);
assert($r === 1);

// 3 10 5 16 8 4 2 1
$r = hailstone(3);
assert($r === 7);

// 4 2 1
$r = hailstone(4);
assert($r === 2);

// 5 16 8 4 2 1
$r = hailstone(5);
assert($r === 5);

echo 'Hailstone of 23061912 is: ', hailstone(23061912), "\n";